//keys that have audio files available
white_keys=['A','S','D','F','G','H','J']
black_keys=['W','E','T','Y','U']
key_audios= {}

//adds keypressed class and plays corresponding audio on keydown
document.addEventListener("keydown",(event) => {
    white_keys.forEach((keyLetter)=>{
        if (event.code.split("Key")[1] === keyLetter){
            document.getElementById(keyLetter).classList.add("key_pressed")
            key_audios[keyLetter]= new Audio("white_keys/"+keyLetter+".mp3");
            key_audios[keyLetter].play();
        }
    })
    black_keys.forEach((keyLetter)=>{
        if (event.code.split("Key")[1] === keyLetter){
            document.getElementById(keyLetter).classList.add("key_pressed")
            key_audios[keyLetter]= new Audio("black_keys/"+keyLetter+".mp3");
            key_audios[keyLetter].play();
        }
    })
})

// on mousedown
function keyClicked(id){
    document.getElementById(id).classList.add("key_pressed")
    if (white_keys.includes(id)){
        key_audios[id]= new Audio("white_keys/"+id+".mp3");
    } else {
        key_audios[id]= new Audio("black_keys/"+id+".mp3");
    }
    key_audios[id].play();
}

//removes the keypressed class on keyup and mousup
document.addEventListener("keyup",(event)=>{
    if(document.getElementById(event.code.split("Key")[1])){
        document.getElementById(event.code.split("Key")[1]).classList.remove("key_pressed")
    }
})
document.addEventListener("mouseup",(event)=>{
    if(document.getElementById(event.path[0].id)) {
        document.getElementById(event.path[0].id).classList.remove("key_pressed")
    }
})
